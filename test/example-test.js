/* global describe it after */
const should = require('chai').should()
const { platform } = require('os')

describe('The SERVER_BIN env var' , function () {
	it('should exist', function () {
		should.exist(process.env.SERVER_BIN)
	})
	it('should be set to the appropriate os specific binary', function () {
		let expectedBinary
		switch (platform()) {
			case 'aix':
				expectedBinary = './server-linux'
				break
			case 'darwin':
				expectedBinary = './server-macos'
				break
			case 'freebsd':
				expectedBinary = './server-linux'
				break
			case 'linux':
				expectedBinary = './server-linux'
				break
			case 'openbsd':
				expectedBinary = './server-linux'
				break
			case 'sunos':
				expectedBinary = './server-linux'
				break
			case 'win32':
				expectedBinary = '.\\server-win.exe'
				break
		}
		process.env.SERVER_BIN.trim().should.equal(expectedBinary)
	})
})