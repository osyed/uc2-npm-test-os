# uc2-npm-test-os

This repo provides a `package.json` with scripts that set the `SERVER_BIN` env var to the appropriate OS-specifc binary, then run the tests:


## To run the example

1. Clone the repo
2. Run `npm install`
2. Run one of the following:
	* `npm run test-win`
	* `npm run test-macos`
	* `npm run test-linux`
